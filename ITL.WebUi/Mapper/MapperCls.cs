﻿using ITL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITL.WebUi.Models;

namespace ITL.WebUi
{
    public static class MapperCls
    {
        public static PersonVM ToView(this Person p)
        {
			  return new PersonVM(p.id, p.Name, p.CPR, p.Roadname, p.PostalCode, p.City, p.HouseNumber, p.Email, p.PhoneNumber, p.Age, p.CreateDate)
			  { id = p.id, Name = p.Name, CPR= p.CPR, Roadname = p.Roadname, PostalCode= p.PostalCode, City = p.City, HouseNumber = p.HouseNumber, Email = p.Email,  PhoneNumber = p.PhoneNumber, Age = p.Age, CreateDate = p.CreateDate};
        }

        public static IEnumerable<PersonVM> ToView(this IEnumerable<Person> ps)
        {
          foreach (var p in ps) yield return p.ToView();
        }


    }
}