﻿
"use strict";

// include plug-ins
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');

var config = {
    //Include all js files but exclude any min.js files
    
    src: ['./Scripts/**/*.js', '!./Scripts/**/*.min.js']
}

var configcss = {
    //Include all js files but exclude any min.js files

    src: ['./Content/**/*.css', '!./Content/**/*.min.css']
}
//delete the output file(s)
gulp.task('clean:js', function () {
    //del is an async function and not a gulp plugin (just standard nodejs)
    //It returns a promise, so make sure you return that from this task function
    //  so gulp knows when the delete is complete
    return del(['./Scripts/all.min.js']);
});

//delete the output file(s)
gulp.task('clean:css', function () {
    //del is an async function and not a gulp plugin (just standard nodejs)
    //It returns a promise, so make sure you return that from this task function
    //  so gulp knows when the delete is complete
    return del(['./Content/all.min.css']);
});


gulp.task("clean", ["clean:js", "clean:css"]);

// Combine and minify all files from the app folder
// This tasks depends on the clean task which means gulp will ensure that the 
// Clean task is completed before running the scripts task.
gulp.task('scripts', function () {

    return gulp.src(config.src)
      .pipe(uglify())
      .pipe(concat('all.min.js'))
      .pipe(gulp.dest('./Scripts'));
});

gulp.task('styles',  function () {

    return gulp.src(configcss.src)
      .pipe(concat('all.min.css'))
      .pipe(gulp.dest('./Content'));
});


//Set a default tasks
 gulp.task('default', ['clean', 'scripts', 'styles'], function () { });

