﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITL.Repository;
using ITL.WebUi.Models;




namespace ITL.WebUi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Admin()
        {
				PersonVM per = new PersonVM();
				ITL.WebUi.WebUiManager webm = new ITL.WebUi.WebUiManager();

				ViewBag.PersonsDropList = new SelectList(webm.Persons(), "Id" , "Name");
				ViewBag.PersonsList = webm.Persons();
				 				
				return View();
        }

		  [HttpPost]
		  public ActionResult Admin(FormCollection f)
		  {
			  ITL.WebUi.WebUiManager webm = new ITL.WebUi.WebUiManager();
			  PersonVM p = new PersonVM();
			  ViewBag.PersonsDropList = new SelectList(webm.Persons(), "Id", "Name");
			  ViewBag.PersonsList = webm.Persons();

			  string value = f["PersonsDropList"].ToString();

			  if ( value != "" )
			  {								 			  			  
				  var i = Convert.ToInt32(value);				 
				  p = webm.PersonGet(i);
				  return RedirectToAction("EditPerson", p);
			  }		  
			    return View();
		  }

        public ActionResult Contact()
        {				
				 Repository.Person p = new Repository.Person();			
            return View(p);
        }

			[HttpPost]
		   public ActionResult Contact(Repository.Person p1)
		 
		  {
			  ITL.Repository.UnitOfWork uow = new Repository.UnitOfWork();
			  TimeSpan CurrentTime =  DateTime.Now.TimeOfDay;
			  p1.CreateDate = CurrentTime;
			 			  			
			  if (ModelState.IsValid)
			  {				 
					  uow.PersonCreate(p1);
					  return RedirectToAction("Confirmation", p1);				 					 				
			  }
		
			  else { return View(p1); }
						 
		  }

			public ActionResult Confirmation(Repository.Person p1)
			{
				return View(p1);
			}

			[HttpGet]
			public ActionResult DropdownPerson(string id)
			{
				ITL.WebUi.WebUiManager webm = new ITL.WebUi.WebUiManager();
				PersonVM p = new PersonVM();
						
					if (!String.IsNullOrEmpty(id))
				{
					var i = Convert.ToInt32(id);
					p = webm.PersonGet(i);

				
					return PartialView("_EditPerson", p);

				}
				else { return PartialView("_DynamicViewEmpty"); }

					



			}

			[HttpPost]
			public ActionResult DropdownPerson( FormCollection fc, Repository.Person p1)
			{

				ITL.WebUi.WebUiManager webm = new ITL.WebUi.WebUiManager();
				PersonVM p = new PersonVM();

				ViewBag.PersonsList = webm.Persons();
				string value = fc["submit"];


				if ((ModelState.IsValid) && (value == "Save"))
				{
					webm.PersonUpdate(p1);
					return RedirectToAction("AdminEdit", p1);
				}

				else { return View(p1); }

			}


			public ActionResult AdminEdit(Repository.Person p1)
			{
				return View(p1);
			}

			

    }
}