﻿$('#PersonsDropList').change(function () {

    /* Get the selected value of dropdownlist */
    var obj = {
        selectedID: $(this).val(),
        command: "PersonsDropList"
    };
    var selectedID = $(this).val();
    /* Request the partial view with .get request. */
    $.get('/Home/DropdownPerson/' + selectedID, function (data) {

        /* data is the pure html returned from action method, load it to your page */
        $('#dynamicpartial').html(data);
        $("form").removeData("validator");
        $("form").removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse("form");
        $('#dynamicpartial').fadeIn('slow');
    });

});