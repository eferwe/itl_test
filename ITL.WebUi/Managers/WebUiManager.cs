﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITL.WebUi.Models;
using ITL.Repository;


namespace ITL.WebUi
{
    public class WebUiManager
    {
        ITL.Repository.UnitOfWork repos = new ITL.Repository.UnitOfWork();

        public PersonVM PersonGet(int id)
        {
            return repos.PersonGet(id).ToView();
        }

        public List<PersonVM> Persons()
        {
            return repos.PersonsGet().ToView().ToList();
        }
        public Person PersonUpdate(Person pvm)
        {

			  return repos.PersonUpdate(pvm);
			  
        }

		 
    }
}