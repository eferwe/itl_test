﻿
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;






namespace ITL.Repository
{
	// using System;
  //	using System.Collections.Generic;
	
	public partial class Person 
	{
		public int id { get; set; }
		[RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "Name can contain only letters")]
		[Required(ErrorMessage = "You must enter Name")]
		public string Name { get; set; }
		[RegularExpression(@"\d{10}", ErrorMessage = "You must give a valid CPR")]
		[Required(ErrorMessage = "You must enter CPR")]
		public string CPR { get; set; }
		[Required(ErrorMessage = "You must enter an Roadname")]
		public string Roadname { get; set; }
		[Required(ErrorMessage = "You must enter zip code")]
		[RegularExpression(@"\d\d\d\d", ErrorMessage = "You must enter a valid PostalCode")]
		public string PostalCode { get; set; }
		[Required(ErrorMessage = "You must enter city")]
		public string City { get; set; } 
		[RegularExpression(@"^[0-9A-Za-z ]{0,8}$", ErrorMessage = "The house number can't be over 8 characters")]
		[Required(ErrorMessage = "You must enter HouseNumber")]
		public string HouseNumber { get; set; }
		[RegularExpression(@"\S+@(\S+\.)+\w{2,4}", ErrorMessage = "There is a problem with the email")]
		// [UniqueEmail(ErrorMessage = "Email is already taken")]
		[Required(ErrorMessage = "You must enter email")]
		public string Email { get; set; }
		[RegularExpression(@"\+{0,1}[\d\s]{8,14}", ErrorMessage = "You must give a valid phone number")]
		[Required(ErrorMessage = "You must enter phone number")]
		public string PhoneNumber { get; set; }
		[RegularExpression(@"\d{1,2}", ErrorMessage = "You must give a valid Age")]
		[Required(ErrorMessage = "You must enter Age")]
		public short Age { get; set; }
		public System.TimeSpan CreateDate { get; set; }
		public Person() { }
		public Person(int id, string Name, string CPR, string Roadname, string PostalCode, string City, string HouseNumber, string Email, string PhoneNumber, short Age, System.TimeSpan CreateDate)
        {
            this.id = id;
            this.Name = Name;
            this.CPR = CPR;
            this.Roadname = Roadname;
				this.PostalCode = PostalCode;
			   this.City = City;
			   this.HouseNumber = HouseNumber;
				this.Email = Email;
				this.PhoneNumber = PhoneNumber;
				this.Age = Age;
				this.CreateDate = CreateDate;

        }
	}
}
